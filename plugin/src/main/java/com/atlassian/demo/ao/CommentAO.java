package com.atlassian.demo.ao;

import com.atlassian.demo.api.Comment;
import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import javax.annotation.Nonnull;

/**
 * @since v1.0
 */
@Table("Comment")
public interface CommentAO extends Entity {

    String AUTHOR = "AUTHOR";
    String CONTENT = "CONTENT";
    String ANNOUNCEMENT_ID = "ANNOUNCEMENT_ID";

    @NotNull
    @Accessor(AUTHOR)
    String getAuthor();

    @NotNull
    @Mutator(AUTHOR)
    void setAuthor(String userKey);

    @NotNull
    @Accessor(CONTENT)
    @StringLength(StringLength.UNLIMITED)
    String getContent();

    @NotNull
    @Indexed
    @Accessor(ANNOUNCEMENT_ID)
    int getAnnouncementId();

    @Nonnull
    static Comment toDomain(@Nonnull CommentAO ao) {
        return new Comment(ao.getID(), ao.getAuthor(), ao.getContent(), ao.getAnnouncementId());
    }
}
