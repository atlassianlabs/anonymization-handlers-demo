package com.atlassian.demo.ao;

import com.atlassian.demo.api.Watcher;
import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import javax.annotation.Nonnull;

/**
 * @since v1.0
 */
@Table("Watcher")
public interface WatcherAO extends Entity {

    String WATCHER = "WATCHER";
    String ANNOUNCEMENT_ID = "ANNOUNCEMENT_ID";

    @NotNull
    @Accessor(WATCHER)
    String getWatcher();

    @NotNull
    @Indexed
    @Accessor(ANNOUNCEMENT_ID)
    int getAnnouncementId();

    @Nonnull
    static Watcher toDomain(@Nonnull WatcherAO ao) {
        return new Watcher(ao.getID(), ao.getWatcher(), ao.getAnnouncementId());
    }
}
