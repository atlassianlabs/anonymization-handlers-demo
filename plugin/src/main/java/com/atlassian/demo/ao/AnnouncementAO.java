package com.atlassian.demo.ao;

import com.atlassian.demo.api.Announcement;
import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.OneToMany;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import javax.annotation.Nonnull;

/**
 * @since v1.0
 */
@Table("Announcement")
public interface AnnouncementAO extends Entity {

    String TITLE = "TITLE";
    String CONTENT = "CONTENT";
    String POINT_OF_CONTACT = "POINT_OF_CONTACT";

    @NotNull
    @Accessor(TITLE)
    String getTitle();

    @NotNull
    @Accessor(CONTENT)
    @StringLength(StringLength.UNLIMITED)
    String getContent();

    @NotNull
    @Accessor(POINT_OF_CONTACT)
    String getPointOfContact();

    @NotNull
    @Mutator(POINT_OF_CONTACT)
    void setPointOfContact(String userKey);

    @OneToMany
    WatcherAO[] getWatchers();

    @OneToMany
    CommentAO[] getComments();

    @Nonnull
    static Announcement toDomain(@Nonnull AnnouncementAO ao) {
        final int id = ao.getID();
        final String title = ao.getTitle();
        final String content = ao.getContent();
        final String pointOfContact = ao.getPointOfContact();
        return new Announcement(id, title, content, pointOfContact);
    }
}
