package com.atlassian.demo.jira.anonymization;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.CommentAO;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import com.atlassian.jira.user.anonymize.UserKeyChangeHandler;
import com.atlassian.jira.user.anonymize.UserPropertyChangeParameter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.atlassian.demo.ao.CommentAO.AUTHOR;
import static java.util.Objects.requireNonNull;

/**
 * Handles user anonymization in Jira by updating author of comments created by the user being anonymized.
 *
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class CommentsAnonymizationHandler implements UserKeyChangeHandler {
    private static final Logger log = LoggerFactory.getLogger(CommentsAnonymizationHandler.class);

    private static final String DESCRIPTION_KEY = "jira.anonymization.demo.comments.affected.entity.name";

    private static final String AUTHOR_CLAUSE = AUTHOR + " = ?";

    private final I18nHelper i18n;
    private final ActiveObjects ao;

    @Inject
    public CommentsAnonymizationHandler(@ComponentImport I18nHelper i18n, @ComponentImport ActiveObjects ao) {
        this.i18n = requireNonNull(i18n);
        this.ao = requireNonNull(ao);
    }

    @Override
    public Collection<AffectedEntity> getAffectedEntities(UserPropertyChangeParameter parameter) {
        final long count = getNumberOfComments(parameter.getOriginal());

        if (count == 0) {
            return Collections.emptyList();
        }
        return Collections.singletonList(AffectedEntity
                .newBuilder(AffectedEntityType.ANONYMIZE)
                .descriptionKey(DESCRIPTION_KEY)
                .numberOfOccurrences(count)
                .build());
    }

    private int getNumberOfComments(final String userKey) {
        return ao.count(CommentAO.class, getAuthorQuery(userKey));
    }

    private Query getAuthorQuery(final String userKey) {
        return Query.select().where(AUTHOR_CLAUSE, userKey);
    }

    @Override
    public ServiceResult update(UserPropertyChangeParameter parameter) {
        final String original = parameter.getOriginal();
        final String target = parameter.getTarget();
        final ErrorHandlingRunner runner = new ErrorHandlingRunner(parameter.getContext(), i18n);

        runner.execute(() -> ao.stream(
                CommentAO.class,
                getAuthorQuery(original),
                comment -> updateComment(target, runner, comment.getID())
        ));

        return runner.getResult();
    }

    private void updateComment(String newAuthorKey, ErrorHandlingRunner runner, int commentId) {
        runner.executeAndUpdateProgress(() -> {
            final CommentAO comment = ao.get(CommentAO.class, commentId);

            if (comment == null) {
                log.warn("Wanted to update author in comment with id {}, but failed to retrieve it", commentId);
                return;
            }

            comment.setAuthor(newAuthorKey);
            comment.save();
        });
    }

    @Override
    public int getNumberOfTasks(UserPropertyChangeParameter parameter) {
        return getNumberOfComments(parameter.getOriginal());
    }
}
