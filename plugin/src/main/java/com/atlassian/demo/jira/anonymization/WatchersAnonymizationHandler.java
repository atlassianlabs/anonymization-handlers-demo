package com.atlassian.demo.jira.anonymization;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.WatcherAO;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import com.atlassian.jira.user.anonymize.UserAnonymizationHandler;
import com.atlassian.jira.user.anonymize.UserAnonymizationParameter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;

import static com.atlassian.demo.ao.WatcherAO.WATCHER;
import static java.util.Objects.requireNonNull;

/**
 * Handles user anonymization in Jira by removing rows from the WATCHER table for the user being anonymized.
 *
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class WatchersAnonymizationHandler implements UserAnonymizationHandler {
    private static final String DESCRIPTION_KEY = "jira.anonymization.demo.watchers.affected.entity.name";

    private static final String WATCHER_CLAUSE = WATCHER + " = ?";

    private final I18nHelper i18n;
    private final ActiveObjects ao;

    @Inject
    public WatchersAnonymizationHandler(@ComponentImport I18nHelper i18n, @ComponentImport ActiveObjects ao) {
        this.i18n = requireNonNull(i18n);
        this.ao = requireNonNull(ao);
    }

    @Override
    public Collection<AffectedEntity> getAffectedEntities(UserAnonymizationParameter parameter) {
        final long count = getNumberOfWatchedEntities(parameter.getUserKey());

        if (count == 0) {
            return Collections.emptyList();
        }
        return Collections.singletonList(AffectedEntity
                .newBuilder(AffectedEntityType.REMOVE)
                .descriptionKey(DESCRIPTION_KEY)
                .numberOfOccurrences(count)
                .build());
    }

    private int getNumberOfWatchedEntities(final String userKey) {
        return ao.count(WatcherAO.class, Query.select().where(WATCHER_CLAUSE, userKey));
    }

    @Override
    public ServiceResult update(UserAnonymizationParameter parameter) {
        final String userKey = parameter.getUserKey();
        final ErrorHandlingRunner runner = new ErrorHandlingRunner(parameter.getContext(), i18n);

        runner.executeAndUpdateProgress(() ->
                ao.deleteWithSQL(
                        WatcherAO.class,
                        WATCHER_CLAUSE,
                        userKey
                ));

        return runner.getResult();
    }

    @Override
    public int getNumberOfTasks(UserAnonymizationParameter parameter) {
        return 1;
    }
}
