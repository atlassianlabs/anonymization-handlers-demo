package com.atlassian.demo.jira.anonymization;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.AnnouncementAO;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import com.atlassian.jira.user.anonymize.OwnershipTransferHandler;
import com.atlassian.jira.user.anonymize.OwnershipTransferParameter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;

import static com.atlassian.demo.ao.AnnouncementAO.POINT_OF_CONTACT;
import static java.util.Objects.requireNonNull;

/**
 * Handles user anonymization in Jira by transferring ownership of Announcements from the user being anonymized.
 *
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class AnnouncementsAnonymizationHandler implements OwnershipTransferHandler {
    private static final String DESCRIPTION_KEY = "jira.anonymization.demo.announcements.affected.entity.name";

    private static final String POINT_OF_CONTACT_CLAUSE = POINT_OF_CONTACT + " = ?";

    private final I18nHelper i18n;
    private final ActiveObjects ao;

    @Inject
    public AnnouncementsAnonymizationHandler(@ComponentImport I18nHelper i18n, @ComponentImport ActiveObjects ao) {
        this.i18n = requireNonNull(i18n);
        this.ao = requireNonNull(ao);
    }

    @Override
    public Collection<AffectedEntity> getAffectedEntities(OwnershipTransferParameter parameter) {
        final long count = getNumberOfAnnouncements(parameter.getCurrentUserKey());

        if (count == 0) {
            return Collections.emptyList();
        }
        return Collections.singletonList(AffectedEntity
                .newBuilder(AffectedEntityType.TRANSFER_OWNERSHIP)
                .descriptionKey(DESCRIPTION_KEY)
                .numberOfOccurrences(count)
                .build());
    }

    private int getNumberOfAnnouncements(final String userKey) {
        return ao.count(AnnouncementAO.class, getPointOfContactQuery(userKey));
    }

    private Query getPointOfContactQuery(final String userKey) {
        return Query.select().where(POINT_OF_CONTACT_CLAUSE, userKey);
    }

    @Override
    public ServiceResult update(OwnershipTransferParameter parameter) {
        final String ownerKey = parameter.getCurrentUserKey();
        final String newOwnerKey = parameter.getTransferToUserKey();
        final ErrorHandlingRunner runner = new ErrorHandlingRunner(parameter.getContext(), i18n);

        final Stream<AnnouncementAO> announcements = runner.execute(() -> Arrays.stream(ao.find(AnnouncementAO.class, getPointOfContactQuery(ownerKey))));
        if (announcements != null) {
            announcements.forEach(announcement -> updateAnnouncement(newOwnerKey, runner, announcement));
        }

        return runner.getResult();
    }

    private void updateAnnouncement(String newOwnerKey, ErrorHandlingRunner runner, AnnouncementAO announcement) {
        runner.executeAndUpdateProgress(() -> {
            announcement.setPointOfContact(newOwnerKey);
            announcement.save();
        });
    }

    @Override
    public int getNumberOfTasks(OwnershipTransferParameter parameter) {
        return getNumberOfAnnouncements(parameter.getCurrentUserKey());
    }
}
