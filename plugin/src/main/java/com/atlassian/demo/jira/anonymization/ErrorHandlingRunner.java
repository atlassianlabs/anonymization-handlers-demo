package com.atlassian.demo.jira.anonymization;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
class ErrorHandlingRunner {
    private static final Logger log = LoggerFactory.getLogger(ErrorHandlingRunner.class);

    private static final String PROCESSING_EXCEPTION_KEY = "change.handler.processing.exception";

    private final Context context;
    private final I18nHelper i18n;
    private final ErrorCollection errorCollection;

    ErrorHandlingRunner(Context context, I18nHelper i18n) {
        this.context = requireNonNull(context);
        this.i18n = requireNonNull(i18n);
        this.errorCollection = new SimpleErrorCollection();
    }

    @Nullable
    <T> T execute(final Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            log.error("There was an exception during handling user anonymization", e);
            errorCollection.addErrorMessage(i18n.getText(PROCESSING_EXCEPTION_KEY, e.getMessage()));
            return null;
        }
    }

    void execute(final Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            log.error("There was an exception during handling user anonymization", e);
            errorCollection.addErrorMessage(i18n.getText(PROCESSING_EXCEPTION_KEY, e.getMessage()));
        }
    }

    void executeAndUpdateProgress(final Runnable runnable) {
        try {
            execute(Executors.callable(runnable, null));
        } finally {
            context.start(null).complete();
        }
    }

    ServiceResult getResult() {
        return new ServiceResultImpl(errorCollection);
    }
}
