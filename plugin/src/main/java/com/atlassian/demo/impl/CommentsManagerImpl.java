package com.atlassian.demo.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.CommentAO;
import com.atlassian.demo.api.Comment;
import com.atlassian.demo.api.CommentsManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.DBParam;
import net.java.ao.Query;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.demo.ao.CommentAO.ANNOUNCEMENT_ID;
import static com.atlassian.demo.ao.CommentAO.AUTHOR;
import static com.atlassian.demo.ao.CommentAO.CONTENT;
import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@Named
@ExportAsService
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class CommentsManagerImpl implements CommentsManager {

    private final ActiveObjects ao;

    @Inject
    public CommentsManagerImpl(@ComponentImport ActiveObjects ao) {
        this.ao = requireNonNull(ao);
    }

    @Override
    public Comment postComment(String authorKey, String content, int announcementId) {
        final DBParam authorParam = new DBParam(AUTHOR, authorKey);
        final DBParam contentParam = new DBParam(CONTENT, content);
        final DBParam announcementParam = new DBParam(ANNOUNCEMENT_ID, announcementId);

        return CommentAO.toDomain(ao.create(CommentAO.class, authorParam, contentParam, announcementParam));
    }

    @Override
    public Optional<Comment> getComment(int commentId) {
        final CommentAO entity = ao.get(CommentAO.class, commentId);
        return entity != null ? Optional.of(CommentAO.toDomain(entity)) : Optional.empty();
    }

    @Override
    public Collection<Comment> getCommentsByAuthor(String userKey) {
        final CommentAO[] entities = ao.find(CommentAO.class,
                Query.select().where("AUTHOR = ?", userKey));

        return Arrays.stream(entities)
                .map(CommentAO::toDomain)
                .collect(Collectors.toList());
    }
}
