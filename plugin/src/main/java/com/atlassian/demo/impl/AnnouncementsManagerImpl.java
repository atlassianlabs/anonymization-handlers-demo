package com.atlassian.demo.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.AnnouncementAO;
import com.atlassian.demo.api.Announcement;
import com.atlassian.demo.api.AnnouncementsManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.DBParam;
import net.java.ao.Query;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.demo.ao.AnnouncementAO.CONTENT;
import static com.atlassian.demo.ao.AnnouncementAO.POINT_OF_CONTACT;
import static com.atlassian.demo.ao.AnnouncementAO.TITLE;
import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@Named
@ExportAsService
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class AnnouncementsManagerImpl implements AnnouncementsManager {

    private final ActiveObjects ao;

    @Inject
    public AnnouncementsManagerImpl(@ComponentImport ActiveObjects ao) {
        this.ao = requireNonNull(ao);
    }

    @Override
    public Announcement postAnnouncement(String title, String content, String pointOfContactKey) {
        final DBParam titleParam = new DBParam(TITLE, title);
        final DBParam contentParam = new DBParam(CONTENT, content);
        final DBParam pointOfContact = new DBParam(POINT_OF_CONTACT, pointOfContactKey);

        return AnnouncementAO.toDomain(ao.create(AnnouncementAO.class, titleParam, contentParam, pointOfContact));
    }

    @Override
    public Optional<Announcement> getAnnouncement(int announcementId) {
        final AnnouncementAO entity = ao.get(AnnouncementAO.class, announcementId);
        return entity != null ? Optional.of(AnnouncementAO.toDomain(entity)) : Optional.empty();
    }

    @Override
    public Collection<Announcement> getAnnouncementsByPointOfContact(String userKey) {
        final AnnouncementAO[] entities = ao.find(AnnouncementAO.class,
                Query.select().where("POINT_OF_CONTACT = ?", userKey));

        return Arrays.stream(entities)
                .map(AnnouncementAO::toDomain)
                .collect(Collectors.toList());
    }
}
