package com.atlassian.demo.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.ao.WatcherAO;
import com.atlassian.demo.api.Announcement;
import com.atlassian.demo.api.AnnouncementsManager;
import com.atlassian.demo.api.Watcher;
import com.atlassian.demo.api.WatchersManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.DBParam;
import net.java.ao.Query;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.demo.ao.WatcherAO.ANNOUNCEMENT_ID;
import static com.atlassian.demo.ao.WatcherAO.WATCHER;
import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@Named
@ExportAsService
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class WatchersManagerImpl implements WatchersManager {

    private final ActiveObjects ao;
    private final AnnouncementsManager announcementsManager;

    @Inject
    public WatchersManagerImpl(@ComponentImport ActiveObjects ao, AnnouncementsManager announcementsManager) {
        this.ao = requireNonNull(ao);
        this.announcementsManager = requireNonNull(announcementsManager);
    }

    @Override
    public Watcher addWatcher(String userKey, int announcementId) {
        final DBParam watcherKey = new DBParam(WATCHER, userKey);
        final DBParam announcement = new DBParam(ANNOUNCEMENT_ID, announcementId);

        return WatcherAO.toDomain(ao.create(WatcherAO.class, watcherKey, announcement));
    }

    @Override
    public Optional<Watcher> getWatch(int watchId) {
        final WatcherAO entity = ao.get(WatcherAO.class, watchId);
        return entity != null ? Optional.of(WatcherAO.toDomain(entity)) : Optional.empty();
    }

    @Override
    public Collection<Announcement> getWatchedAnnouncementsForUser(String userKey) {
        final WatcherAO[] entities = ao.find(WatcherAO.class,
                Query.select().where("WATCHER = ?", userKey));

        return Arrays.stream(entities)
                .map(watcher -> announcementsManager.getAnnouncement(watcher.getAnnouncementId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
