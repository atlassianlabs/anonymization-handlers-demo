package com.atlassian.demo.api;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class Watcher {
    private final int id;
    private final String watcher;
    private final int announcementId;

    public Watcher(int id, String watcher, int announcementId) {
        this.id = id;
        this.watcher = requireNonNull(watcher);
        this.announcementId = announcementId;
    }

    public int getId() {
        return id;
    }

    public String getWatcher() {
        return watcher;
    }

    public int getAnnouncementId() {
        return announcementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Watcher watcher = (Watcher) o;
        return id == watcher.id &&
                announcementId == watcher.announcementId &&
                Objects.equals(this.watcher, watcher.watcher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, watcher, announcementId);
    }

    @Override
    public String toString() {
        return "Watcher{" +
                "id=" + id +
                ", watcher='" + watcher + '\'' +
                ", announcementId=" + announcementId +
                '}';
    }
}
