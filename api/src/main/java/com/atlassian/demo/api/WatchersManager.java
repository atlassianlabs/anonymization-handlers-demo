package com.atlassian.demo.api;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Optional;

/**
 * A basic manager allowing us to create and retrieve data required for the sake of the demo.
 *
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface WatchersManager {

    Watcher addWatcher(String userKey, int announcementId);

    Optional<Watcher> getWatch(int watchId);

    Collection<Announcement> getWatchedAnnouncementsForUser(String userKey);
}
