package com.atlassian.demo.api;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Optional;

/**
 * A basic manager allowing us to create and retrieve data required for the sake of the demo.
 *
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public interface CommentsManager {

    Comment postComment(String authorKey, String content, int announcementId);

    Optional<Comment> getComment(int commentId);

    Collection<Comment> getCommentsByAuthor(String userKey);
}
