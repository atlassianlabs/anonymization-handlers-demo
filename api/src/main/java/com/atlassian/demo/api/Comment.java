package com.atlassian.demo.api;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class Comment {
    private final int id;
    private final String author;
    private final String content;
    private final int announcementId;

    public Comment(int id, String author, String content, int announcementId) {
        this.id = id;
        this.author = requireNonNull(author);
        this.content = requireNonNull(content);
        this.announcementId = announcementId;
    }

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public int getAnnouncementId() {
        return announcementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id &&
                announcementId == comment.announcementId &&
                Objects.equals(author, comment.author) &&
                Objects.equals(content, comment.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, content, announcementId);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", announcementId=" + announcementId +
                '}';
    }
}
