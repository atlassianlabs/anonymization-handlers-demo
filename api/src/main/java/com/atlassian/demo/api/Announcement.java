package com.atlassian.demo.api;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * @since v1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class Announcement {
    private final int id;
    private final String title;
    private final String content;
    private final String pointOfContactKey;

    public Announcement(int id, String title, String content, String pointOfContactKey) {
        this.id = id;
        this.title = requireNonNull(title);
        this.content = requireNonNull(content);
        this.pointOfContactKey = requireNonNull(pointOfContactKey);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getPointOfContactKey() {
        return pointOfContactKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Announcement that = (Announcement) o;
        return id == that.id &&
                Objects.equals(title, that.title) &&
                Objects.equals(content, that.content) &&
                Objects.equals(pointOfContactKey, that.pointOfContactKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, pointOfContactKey);
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", pointOfContactKey='" + pointOfContactKey + '\'' +
                '}';
    }
}
