package com.atlassian.demo.testkit;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class Product {
    private static final Product CONFLUENCE = new Product("Confluence", "confluence", "/confluence", 1990);
    private static final Product JIRA = new Product("Jira", "jira", "/jira", 2990);
    private static final Product FECRU = new Product("FishEye", "fecru", "/fecru", 3990);
    private static final Product CROWD = new Product("Crowd", "crowd", "/crowd", 4990);
    private static final Product REFAPP = new Product("RefApp", "refapp", "/refapp", 5990);
    private static final Product BAMBOO = new Product("Bamboo", "bamboo", "/bamboo", 6990);
    private static final Product BITBUCKET = new Product("Bitbucket", "bitbucket", "/bitbucket", 7990);

    private static final Set<Product> KNOWN_PRODUCTS = ImmutableSet.of(CONFLUENCE, JIRA, FECRU, CROWD, REFAPP, BAMBOO, BITBUCKET);
    private static final Product CURRENT;
    private static final Pattern BASE_URL_PATTERN = Pattern.compile(".*:([0-9]{4,5})(/[^/]+).*");
    private static final String VERSION;

    static {
        // Try to obtain the product by checking for system property, or base URL. If neither are found default to Jira.
        final String productId = ofNullable(getProperty("product"))
                .orElseThrow(() -> new IllegalStateException("System property 'product' must be specified!"));
        final Product defaultProduct = forInstanceId(productId).orElse(JIRA);
        final String productBaseUrl = getProperty(format("baseurl.%s", productId));

        if (!isEmpty(productBaseUrl)) {
            try {
                final URL url = new URL(productBaseUrl);
                CURRENT = new Product(defaultProduct.name, productId, url.getPath(), url.getPort());
                CURRENT.baseUrl = url.toString();
                System.setProperty(format("http.%s.port", productId), String.valueOf(url.getPort()));
                System.setProperty(format("context.%s.path", productId), url.getPath());
            } catch (MalformedURLException e) {
                throw new RuntimeException("Invalid base url for product specified: " + productBaseUrl, e);
            }
        } else {
            CURRENT = forBaseUrl(getProperty("baseurl")).orElse(defaultProduct);
        }
        VERSION = getProperty(format("%s.version", CURRENT.instanceId));
    }

    private final String name;
    private final String instanceId;
    private final String defaultContextPath;
    private final int defaultHttpPort;
    private String baseUrl;

    private Product(String name, String instanceId, String defaultContextPath, int defaultHttpPort) {
        this.name = requireNonNull(name);
        this.instanceId = requireNonNull(instanceId);
        this.defaultContextPath = requireNonNull(defaultContextPath);
        this.defaultHttpPort = defaultHttpPort;
    }

    static Product currentProduct() {
        return CURRENT;
    }

    private static Optional<Product> forInstanceId(@Nonnull String instanceId) {
        return KNOWN_PRODUCTS.stream()
                .filter(p -> instanceId.equals(p.getInstanceId()))
                .findFirst();
    }

    private static Optional<Product> forBaseUrl(String baseUrl) {
        if (isBlank(baseUrl)) {
            return Optional.empty();
        }

        Matcher matcher = BASE_URL_PATTERN.matcher(baseUrl);
        if (matcher.matches()) {
            String contextPath = matcher.group(2);
            for (Product product : KNOWN_PRODUCTS) {
                if (product.defaultContextPath.equalsIgnoreCase(contextPath)) {
                    product.baseUrl = baseUrl;
                    return Optional.of(product);
                }
            }
        }

        return Optional.empty();
    }

    public static String getVersion() {
        return VERSION;
    }

    public String getName() {
        return name;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String getDefaultContextPath() {
        return defaultContextPath;
    }

    public int getDefaultHttpPort() {
        return defaultHttpPort;
    }

    public String getBaseUrl() {
        return isEmpty(baseUrl) ? "http://localhost:" + defaultHttpPort + defaultContextPath : baseUrl;
    }

    public String getAbsoluteUrl(String relativeUrl) {
        return getBaseUrl() + relativeUrl;
    }
}
