package com.atlassian.demo.testkit.control;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import io.restassured.http.ContentType;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class CommentsControl {

    private final AnonymizationHandlersBackdoor backdoor;

    public CommentsControl(AnonymizationHandlersBackdoor backdoor) {
        this.backdoor = backdoor;
    }

    public Comment getComment(int id) {
        return backdoor
                .request()
                .get("comment/{id}", id)
                .as(Comment.class);
    }

    public List<Comment> getCommentsByAuthor(String userKey) {
        return Arrays.asList(backdoor
                .request()
                .queryParam("author", userKey)
                .get("comment")
                .as(Comment[].class));
    }

    public Comment postComment(Comment comment) {
        return backdoor
                .request()
                .contentType(ContentType.JSON)
                .body(comment)
                .post("comment")
                .as(Comment.class);
    }

    public static class Comment {
        private Integer id;
        private String author;
        private String content;
        private Integer announcementId;

        // for Jackson
        private Comment() {
        }

        public Comment(String author, String content, Integer announcementId) {
            this(0, author, content, announcementId);
        }

        public Comment(Integer id, String author, String content, Integer announcementId) {
            this.id = id;
            this.author = author;
            this.content = content;
            this.announcementId = announcementId;
        }

        public Integer getId() {
            return id;
        }

        public String getAuthor() {
            return author;
        }

        public String getContent() {
            return content;
        }

        public Integer getAnnouncementId() {
            return announcementId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Comment comment = (Comment) o;
            return Objects.equals(id, comment.id) &&
                    Objects.equals(content, comment.content) &&
                    Objects.equals(announcementId, comment.announcementId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, content, announcementId);
        }

        @Override
        public String toString() {
            return "Comment{" +
                    "id=" + id +
                    ", author='" + author + '\'' +
                    ", content='" + content + '\'' +
                    ", announcementId=" + announcementId +
                    '}';
        }
    }
}
