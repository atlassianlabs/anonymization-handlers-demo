package com.atlassian.demo.testkit.control;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import io.restassured.http.ContentType;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class AnnouncementsControl {

    private final AnonymizationHandlersBackdoor backdoor;

    public AnnouncementsControl(AnonymizationHandlersBackdoor backdoor) {
        this.backdoor = backdoor;
    }

    public Announcement getAnnouncement(int id) {
        return backdoor
                .request()
                .get("announcement/{id}", id)
                .as(Announcement.class);
    }

    public List<Announcement> getAnnouncementsForPointOfContact(String userKey) {
        return Arrays.asList(backdoor
                .request()
                .queryParam("pointOfContact", userKey)
                .get("announcement")
                .as(Announcement[].class));
    }

    public Announcement postAnnouncement(Announcement announcement) {
        return backdoor
                .request()
                .contentType(ContentType.JSON)
                .body(announcement)
                .post("announcement")
                .as(Announcement.class);
    }

    public static class Announcement {
        private Integer id;
        private String title;
        private String content;
        private String pointOfContactKey;

        // for Jackson
        private Announcement() {
        }

        public Announcement(String title, String content, String pointOfContactKey) {
            this(0, title, content, pointOfContactKey);
        }

        public Announcement(Integer id, String title, String content, String pointOfContactKey) {
            this.id = id;
            this.title = title;
            this.content = content;
            this.pointOfContactKey = pointOfContactKey;
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }

        public String getPointOfContactKey() {
            return pointOfContactKey;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Announcement that = (Announcement) o;
            return Objects.equals(id, that.id) &&
                    Objects.equals(title, that.title) &&
                    Objects.equals(content, that.content);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, title, content);
        }

        @Override
        public String toString() {
            return "Announcement{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    ", content='" + content + '\'' +
                    ", pointOfContactKey='" + pointOfContactKey + '\'' +
                    '}';
        }
    }
}
