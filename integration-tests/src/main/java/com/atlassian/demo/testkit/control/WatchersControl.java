package com.atlassian.demo.testkit.control;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import io.restassured.http.ContentType;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class WatchersControl {

    private final AnonymizationHandlersBackdoor backdoor;

    public WatchersControl(AnonymizationHandlersBackdoor backdoor) {
        this.backdoor = backdoor;
    }

    public Watcher getWatch(int id) {
        return backdoor
                .request()
                .get("watcher/{id}", id)
                .as(Watcher.class);
    }

    public List<Announcement> getWatchedAnnouncementsForUser(String userKey) {
        return Arrays.asList(backdoor
                .request()
                .queryParam("watcher", userKey)
                .get("watcher")
                .as(Announcement[].class));
    }

    public Watcher addWatch(Watcher watcher) {
        return backdoor
                .request()
                .contentType(ContentType.JSON)
                .body(watcher)
                .post("watcher")
                .as(Watcher.class);
    }

    public static class Watcher {
        private Integer id;
        private String watcher;
        private Integer announcementId;

        // for Jackson
        private Watcher() {
        }

        public Watcher(String watcher, Integer announcementId) {
            this(0, watcher, announcementId);
        }

        public Watcher(Integer id, String watcher, Integer announcementId) {
            this.id = id;
            this.watcher = watcher;
            this.announcementId = announcementId;
        }

        public Integer getId() {
            return id;
        }

        public String getWatcher() {
            return watcher;
        }

        public Integer getAnnouncementId() {
            return announcementId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Watcher watcher1 = (Watcher) o;
            return Objects.equals(id, watcher1.id) &&
                    Objects.equals(watcher, watcher1.watcher) &&
                    Objects.equals(announcementId, watcher1.announcementId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, watcher, announcementId);
        }

        @Override
        public String toString() {
            return "Watcher{" +
                    "id=" + id +
                    ", watcher='" + watcher + '\'' +
                    ", announcementId=" + announcementId +
                    '}';
        }
    }
}
