package com.atlassian.demo.testkit;

import com.atlassian.demo.testkit.control.AnnouncementsControl;
import com.atlassian.demo.testkit.control.AnonymizeControl;
import com.atlassian.demo.testkit.control.CommentsControl;
import com.atlassian.demo.testkit.control.WatchersControl;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

import static com.atlassian.demo.testkit.Product.currentProduct;

public class AnonymizationHandlersBackdoor {

    private static final String REST_BASE_PATH = "rest/anonymization-handlers-backdoor/1/";

    private final Product product = currentProduct();

    public Product getProduct() {
        return product;
    }

    public AnnouncementsControl announcements() {
        return new AnnouncementsControl(this);
    }

    public CommentsControl comments() {
        return new CommentsControl(this);
    }

    public WatchersControl watchers() {
        return new WatchersControl(this);
    }

    public AnonymizeControl anonymize() {
        return new AnonymizeControl(this);
    }

    public RequestSpecification request() {
        return RestAssured.given().baseUri(product.getBaseUrl()).basePath(REST_BASE_PATH);
    }
}
