package com.atlassian.demo.testkit.control;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class AnonymizeControl {

    private final AnonymizationHandlersBackdoor backdoor;

    public AnonymizeControl(AnonymizationHandlersBackdoor backdoor) {
        this.backdoor = backdoor;
    }

    public List<AffectedEntity> getAffectedEntities(String userKey) {
        return Arrays.asList(request()
                .queryParam("userKey", userKey)
                .get("anonymize")
                .as(AffectedEntity[].class));
    }

    public AnonymizeResult anonymizeUser(String userKey, String newOwnerKey) {
        return request()
                .contentType(ContentType.JSON)
                .queryParam("userKey", userKey)
                .queryParam("newOwnerKey", newOwnerKey)
                .post("anonymize")
                .as(AnonymizeResult.class);
    }

    // instead of overriding behavior from AnonymizationHandlersBackdoor, we could create Jira-specific backdoor class
    private RequestSpecification request() {
        return RestAssured.given().baseUri(backdoor.getProduct().getBaseUrl())
                .auth().preemptive().basic("admin", "admin")
                .basePath("rest/anonymization-handlers-jira-backdoor/1/");
    }

    public static class AffectedEntity {
        private AffectedEntityType type;
        private String descriptionKey;
        private Long numberOfOccurrences;
        private String uriDisplayNameKey;
        private String uri;

        // for Jackson
        private AffectedEntity() {
        }

        public AffectedEntity(AffectedEntityType type, String descriptionKey, Long numberOfOccurrences) {
            this.type = type;
            this.descriptionKey = descriptionKey;
            this.numberOfOccurrences = numberOfOccurrences;
        }

        public AffectedEntityType getType() {
            return type;
        }

        public String getDescriptionKey() {
            return descriptionKey;
        }

        public Long getNumberOfOccurrences() {
            return numberOfOccurrences;
        }

        public String getUriDisplayNameKey() {
            return uriDisplayNameKey;
        }

        public String getUri() {
            return uri;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AffectedEntity that = (AffectedEntity) o;
            return type == that.type &&
                    Objects.equals(descriptionKey, that.descriptionKey) &&
                    Objects.equals(numberOfOccurrences, that.numberOfOccurrences) &&
                    Objects.equals(uriDisplayNameKey, that.uriDisplayNameKey) &&
                    Objects.equals(uri, that.uri);
        }

        @Override
        public int hashCode() {
            return Objects.hash(type, descriptionKey, numberOfOccurrences, uriDisplayNameKey, uri);
        }

        @Override
        public String toString() {
            return "AffectedEntity{" +
                    "type=" + type +
                    ", descriptionKey='" + descriptionKey + '\'' +
                    ", numberOfOccurrences=" + numberOfOccurrences +
                    ", uriDisplayNameKey='" + uriDisplayNameKey + '\'' +
                    ", uri='" + uri + '\'' +
                    '}';
        }
    }

    public static class AnonymizeResult {
        private String userKey;
        private String newUserKey;
        private String newUserName;

        // for Jackson
        private AnonymizeResult() {
        }

        public String getUserKey() {
            return userKey;
        }

        public String getNewUserKey() {
            return newUserKey;
        }

        public String getNewUserName() {
            return newUserName;
        }
    }
}
