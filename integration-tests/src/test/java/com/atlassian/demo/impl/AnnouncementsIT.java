package com.atlassian.demo.impl;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class AnnouncementsIT {
    private static final AnonymizationHandlersBackdoor backdoor = new AnonymizationHandlersBackdoor();

    private static Announcement announcement1;
    private static Announcement announcement2;
    private static Announcement announcement3;

    @BeforeClass // ideally we'd clean up after ourselves and create entities before each test
    public static void setUp() {
        announcement1 = backdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "admin"));
        announcement2 = backdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "other_user"));
        announcement3 = backdoor.announcements()
                .postAnnouncement(new Announcement("title3", "content3", "admin"));
    }

    @Test
    @Ignore("fails on re-run, because we don't clean up data between tests :(")
    public void someSillyTestJustToShowThatThingsWork() {
        List<Announcement> actual = backdoor.announcements().getAnnouncementsForPointOfContact("admin");
        assertThat(actual, containsInAnyOrder(announcement1, announcement3));

        actual = backdoor.announcements().getAnnouncementsForPointOfContact("other_user");
        assertThat(actual, containsInAnyOrder(announcement2));
    }

    @Test
    public void anotherSillyTestJustToShowThatThingsWork() {
        final Announcement actual = backdoor.announcements().getAnnouncement(announcement1.getId());

        assertThat(actual, equalTo(announcement1));
    }
}
