package com.atlassian.demo.impl;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import com.atlassian.demo.testkit.control.CommentsControl.Comment;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class CommentsIT {
    private static final AnonymizationHandlersBackdoor backdoor = new AnonymizationHandlersBackdoor();

    private static Comment comment1;
    private static Comment comment2;
    private static Comment comment3;
    private static Comment comment4;

    @BeforeClass // ideally we'd clean up after ourselves and create entities before each test
    public static void setUp() {
        final Announcement announcement1 = backdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "fred"));
        final Announcement announcement2 = backdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "bob"));

        comment1 = backdoor.comments().postComment(new Comment("admin", "content1", announcement1.getId()));
        comment2 = backdoor.comments().postComment(new Comment("admin", "content2", announcement1.getId()));
        comment3 = backdoor.comments().postComment(new Comment("other_user", "content3", announcement1.getId()));
        comment4 = backdoor.comments().postComment(new Comment("admin", "content4", announcement2.getId()));
    }

    @Test
    @Ignore("fails on re-run, because we don't clean up data between tests :(")
    public void someSillyTestJustToShowThatThingsWork() {
        List<Comment> actual = backdoor.comments().getCommentsByAuthor("admin");
        assertThat(actual, containsInAnyOrder(comment1, comment2, comment4));

        actual = backdoor.comments().getCommentsByAuthor("other_user");
        assertThat(actual, containsInAnyOrder(comment3));
    }

    @Test
    public void anotherSillyTestJustToShowThatThingsWork() {
        final Comment actual = backdoor.comments().getComment(comment3.getId());

        assertThat(actual, equalTo(comment3));
    }
}
