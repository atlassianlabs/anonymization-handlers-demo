package com.atlassian.demo.impl;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import com.atlassian.demo.testkit.control.WatchersControl.Watcher;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class WatchersIT {
    private static final AnonymizationHandlersBackdoor backdoor = new AnonymizationHandlersBackdoor();

    private static Announcement announcement1;
    private static Announcement announcement2;

    private static Watcher watch1;
    private static Watcher watch2;
    private static Watcher watch3;

    @BeforeClass // ideally we'd clean up after ourselves and create entities before each test
    public static void setUp() {
        announcement1 = backdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "fred"));
        announcement2 = backdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "bob"));

        watch1 = backdoor.watchers().addWatch(new Watcher("admin", announcement1.getId()));
        watch2 = backdoor.watchers().addWatch(new Watcher("other_user", announcement1.getId()));
        watch3 = backdoor.watchers().addWatch(new Watcher("admin", announcement2.getId()));
    }

    @Test
    @Ignore("fails on re-run, because we don't clean up data between tests :(")
    public void someSillyTestJustToShowThatThingsWork() {
        List<Announcement> actual = backdoor.watchers().getWatchedAnnouncementsForUser("admin");
        assertThat(actual, containsInAnyOrder(announcement1, announcement2));

        actual = backdoor.watchers().getWatchedAnnouncementsForUser("other_user");
        assertThat(actual, containsInAnyOrder(announcement1));
    }

    @Test
    public void anotherSillyTestJustToShowThatThingsWork() {
        final Watcher actual = backdoor.watchers().getWatch(watch2.getId());

        assertThat(actual, equalTo(watch2));
    }
}
