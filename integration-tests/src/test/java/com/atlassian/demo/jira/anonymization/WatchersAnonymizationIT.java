package com.atlassian.demo.jira.anonymization;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import com.atlassian.demo.testkit.control.AnonymizeControl.AffectedEntity;
import com.atlassian.demo.testkit.control.AnonymizeControl.AnonymizeResult;
import com.atlassian.demo.testkit.control.WatchersControl.Watcher;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.user.anonymize.AffectedEntityType.REMOVE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

@RestoreBlankInstance
public class WatchersAnonymizationIT extends BaseJiraRestTest {
    private static final AnonymizationHandlersBackdoor handlersBackdoor = new AnonymizationHandlersBackdoor();

    private static final String USER_NAME = "some user name";
    private String userKey;

    @Before
    public void setUpClass() {
        userKey = backdoor.usersAndGroups().addUser(USER_NAME).getUserByName(USER_NAME).getKey();
    }

    @Test
    public void getAffectedEntitiesReturnsNothingWhenNoWatches() {
        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities.size(), equalTo(0));
    }

    @Test
    public void getAffectedEntitiesReturnsWatches() {
        final Announcement announcement1 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "admin"));
        final Announcement announcement2 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "fred"));

        handlersBackdoor.watchers().addWatch(new Watcher("admin", announcement1.getId()));
        handlersBackdoor.watchers().addWatch(new Watcher(userKey, announcement1.getId()));
        handlersBackdoor.watchers().addWatch(new Watcher(userKey, announcement2.getId()));

        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities, hasItem(new AffectedEntity(
                REMOVE,
                "jira.anonymization.demo.watchers.affected.entity.name",
                2L
        )));
    }

    @Test
    public void anonymizeDeletesWatches() {
        final Announcement announcement1 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "admin"));
        final Announcement announcement2 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "fred"));

        handlersBackdoor.watchers().addWatch(new Watcher("admin", announcement1.getId()));
        handlersBackdoor.watchers().addWatch(new Watcher(userKey, announcement1.getId()));
        handlersBackdoor.watchers().addWatch(new Watcher(userKey, announcement2.getId()));

        final List<Announcement> watched = handlersBackdoor.watchers().getWatchedAnnouncementsForUser(userKey);
        assertThat(watched.size(), equalTo(2));

        final AnonymizeResult anonymizeResult = handlersBackdoor.anonymize().anonymizeUser(userKey, "admin");
        assertThat(handlersBackdoor.watchers().getWatchedAnnouncementsForUser(userKey).size(), equalTo(0));
        assertThat(handlersBackdoor.watchers().getWatchedAnnouncementsForUser(anonymizeResult.getNewUserKey()).size(), equalTo(0));
    }
}
