package com.atlassian.demo.jira.anonymization;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import com.atlassian.demo.testkit.control.AnonymizeControl.AffectedEntity;
import com.atlassian.demo.testkit.control.AnonymizeControl.AnonymizeResult;
import com.atlassian.demo.testkit.control.WatchersControl.Watcher;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.user.anonymize.AffectedEntityType.REMOVE;
import static com.atlassian.jira.user.anonymize.AffectedEntityType.TRANSFER_OWNERSHIP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

@RestoreBlankInstance
public class AnnouncementsAnonymizationIT extends BaseJiraRestTest {
    private static final AnonymizationHandlersBackdoor handlersBackdoor = new AnonymizationHandlersBackdoor();

    private static final String USER_NAME = "some user name";
    private String userKey;

    @Before
    public void setUpClass() {
        userKey = backdoor.usersAndGroups().addUser(USER_NAME).getUserByName(USER_NAME).getKey();
    }

    @Test
    public void getAffectedEntitiesReturnsNothingWhenNoAnnouncements() {
        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities.size(), equalTo(0));
    }

    @Test
    public void getAffectedEntitiesReturnsAnnouncements() {
        handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", userKey));
        handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "admin"));
        handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title3", "content3", userKey));

        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities, hasItem(new AffectedEntity(
                TRANSFER_OWNERSHIP,
                "jira.anonymization.demo.announcements.affected.entity.name",
                2L
        )));
    }

    @Test
    public void anonymizeTransfersAnnouncements() {
        final Announcement announcement1 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", userKey));
        final Announcement announcement2 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title3", "content3", userKey));

        final List<Announcement> announcements = handlersBackdoor.announcements().getAnnouncementsForPointOfContact(userKey);
        assertThat(announcements.size(), equalTo(2));

        final AnonymizeResult anonymizeResult = handlersBackdoor.anonymize().anonymizeUser(userKey, "admin");

        final List<Announcement> oldUserKey = handlersBackdoor.announcements().getAnnouncementsForPointOfContact(userKey);
        assertThat(oldUserKey.size(), equalTo(0));
        final List<Announcement> newUserKey = handlersBackdoor.announcements().getAnnouncementsForPointOfContact(anonymizeResult.getNewUserKey());
        assertThat(newUserKey.size(), equalTo(0));
        final List<Announcement> transferredTo = handlersBackdoor.announcements().getAnnouncementsForPointOfContact("admin");
        assertThat(transferredTo, containsInAnyOrder(announcement1, announcement2));
    }
}
