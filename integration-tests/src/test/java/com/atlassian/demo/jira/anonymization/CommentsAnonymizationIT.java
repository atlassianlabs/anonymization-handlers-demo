package com.atlassian.demo.jira.anonymization;

import com.atlassian.demo.testkit.AnonymizationHandlersBackdoor;
import com.atlassian.demo.testkit.control.AnnouncementsControl.Announcement;
import com.atlassian.demo.testkit.control.AnonymizeControl.AffectedEntity;
import com.atlassian.demo.testkit.control.AnonymizeControl.AnonymizeResult;
import com.atlassian.demo.testkit.control.CommentsControl;
import com.atlassian.demo.testkit.control.CommentsControl.Comment;
import com.atlassian.demo.testkit.control.WatchersControl.Watcher;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.user.anonymize.AffectedEntityType.ANONYMIZE;
import static com.atlassian.jira.user.anonymize.AffectedEntityType.REMOVE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

@RestoreBlankInstance
public class CommentsAnonymizationIT extends BaseJiraRestTest {
    private static final AnonymizationHandlersBackdoor handlersBackdoor = new AnonymizationHandlersBackdoor();

    private String userKey;

    @Before
    public void setUpClass() {
        userKey = backdoor.usersAndGroups().getUserByName("fred").getKey();
    }

    @Test
    public void getAffectedEntitiesReturnsNothingWhenNoComments() {
        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities.size(), equalTo(1)); // 1 for user profile
    }

    @Test
    public void getAffectedEntitiesReturnsComments() {
        final Announcement announcement1 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "admin"));
        final Announcement announcement2 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "admin"));

        handlersBackdoor.comments().postComment(new Comment(userKey, "content1", announcement1.getId()));
        handlersBackdoor.comments().postComment(new Comment(userKey, "content2", announcement1.getId()));
        handlersBackdoor.comments().postComment(new Comment(userKey, "content3", announcement2.getId()));
        handlersBackdoor.comments().postComment(new Comment("admin", "content4", announcement2.getId()));
        handlersBackdoor.comments().postComment(new Comment(userKey, "content5", announcement2.getId()));

        final List<AffectedEntity> affectedEntities = handlersBackdoor.anonymize().getAffectedEntities(userKey);

        assertThat(affectedEntities, hasItem(new AffectedEntity(
                ANONYMIZE,
                "jira.anonymization.demo.comments.affected.entity.name",
                4L
        )));
    }

    @Test
    public void anonymizeUpdatesComments() {
        final Announcement announcement1 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title1", "content1", "admin"));
        final Announcement announcement2 = handlersBackdoor.announcements()
                .postAnnouncement(new Announcement("title2", "content2", "admin"));

        final Comment comment1 = handlersBackdoor.comments().postComment(new Comment(userKey, "content1", announcement1.getId()));
        final Comment comment2 = handlersBackdoor.comments().postComment(new Comment(userKey, "content2", announcement1.getId()));
        final Comment comment3 = handlersBackdoor.comments().postComment(new Comment(userKey, "content3", announcement2.getId()));
        final Comment comment4 = handlersBackdoor.comments().postComment(new Comment(userKey, "content4", announcement2.getId()));
        final Comment comment5 = handlersBackdoor.comments().postComment(new Comment(userKey, "content5", announcement2.getId()));

        final List<Comment> comments = handlersBackdoor.comments().getCommentsByAuthor(userKey);
        assertThat(comments.size(), equalTo(5));

        final AnonymizeResult anonymizeResult = handlersBackdoor.anonymize().anonymizeUser(userKey, "admin");
        assertThat(handlersBackdoor.comments().getCommentsByAuthor(userKey).size(), equalTo(0));
        assertThat(handlersBackdoor.comments().getCommentsByAuthor(anonymizeResult.getNewUserKey()),
                containsInAnyOrder(comment1, comment2, comment3, comment4, comment5));
    }
}
