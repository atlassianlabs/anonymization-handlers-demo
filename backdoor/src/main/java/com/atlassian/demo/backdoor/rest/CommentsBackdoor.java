package com.atlassian.demo.backdoor.rest;

import com.atlassian.demo.api.Comment;
import com.atlassian.demo.api.CommentsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("comment")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class CommentsBackdoor {
    private final CommentsManager commentsManager;

    @Inject
    public CommentsBackdoor(@ComponentImport CommentsManager commentsManager) {
        this.commentsManager = commentsManager;
    }

    @GET
    @Path("{id}")
    public CommentRest getComment(@PathParam("id") int id) {
        final Optional<Comment> comment = commentsManager.getComment(id);
        return comment.map(CommentRest::new).orElseGet(CommentRest::new);
    }

    @GET
    public Collection<CommentRest> getCommentsByAuthor(@QueryParam("author") String userKey) {
        final Collection<Comment> comments = commentsManager.getCommentsByAuthor(userKey);
        return comments.stream().map(CommentRest::new).collect(Collectors.toList());
    }

    @POST
    public CommentRest postComment(CommentRest comment) {
        final Comment created = commentsManager.postComment(
                comment.getAuthor(),
                comment.getContent(),
                comment.getAnnouncementId()
        );
        return getComment(created.getId());
    }

    @JsonAutoDetect
    public static class CommentRest {
        private int id;
        private String author;
        private String content;
        private Integer announcementId;

        // for Jackson
        private CommentRest() {
        }

        public CommentRest(Comment comment) {
            this.id = comment.getId();
            this.author = comment.getAuthor();
            this.content = comment.getContent();
            this.announcementId = comment.getAnnouncementId();
        }

        public int getId() {
            return id;
        }

        public String getAuthor() {
            return author;
        }

        public String getContent() {
            return content;
        }

        public Integer getAnnouncementId() {
            return announcementId;
        }
    }
}
