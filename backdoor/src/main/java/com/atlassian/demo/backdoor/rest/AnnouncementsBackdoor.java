package com.atlassian.demo.backdoor.rest;

import com.atlassian.demo.api.Announcement;
import com.atlassian.demo.api.AnnouncementsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("announcement")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class AnnouncementsBackdoor {
    private final AnnouncementsManager announcementsManager;

    @Inject
    public AnnouncementsBackdoor(@ComponentImport AnnouncementsManager announcementsManager) {
        this.announcementsManager = announcementsManager;
    }

    @GET
    @Path("{id}")
    public AnnouncementRest getAnnouncement(@PathParam("id") int id) {
        final Optional<Announcement> announcement = announcementsManager.getAnnouncement(id);
        return announcement.map(AnnouncementRest::new).orElseGet(AnnouncementRest::new);
    }

    @GET
    public Collection<AnnouncementRest> getAnnouncements(@QueryParam("pointOfContact") String userKey) {
        final Collection<Announcement> announcements = announcementsManager.getAnnouncementsByPointOfContact(userKey);
        return announcements.stream().map(AnnouncementRest::new).collect(Collectors.toList());
    }

    @POST
    public AnnouncementRest postAnnouncement(AnnouncementRest announcement) {
        final Announcement created = announcementsManager.postAnnouncement(
                announcement.getTitle(),
                announcement.getContent(),
                announcement.getPointOfContactKey()
        );
        return getAnnouncement(created.getId());
    }

    @JsonAutoDetect
    public static class AnnouncementRest {
        private Integer id;
        private String title;
        private String content;
        private String pointOfContactKey;

        // for Jackson
        private AnnouncementRest() {
        }

        public AnnouncementRest(Announcement announcement) {
            this.id = announcement.getId() != 0 ? announcement.getId() : null;
            this.title = announcement.getTitle();
            this.content = announcement.getContent();
            this.pointOfContactKey = announcement.getPointOfContactKey();
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }

        public String getPointOfContactKey() {
            return pointOfContactKey;
        }
    }
}
