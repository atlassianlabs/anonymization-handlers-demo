package com.atlassian.demo.backdoor.jira.rest;

import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AffectedEntityLink;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.annotation.Nonnull;
import java.io.Serializable;

@JsonAutoDetect
public class AffectedEntityBean implements Serializable {
    private AffectedEntityType type;
    private String descriptionKey;
    private Long numberOfOccurrences;
    private String uriDisplayNameKey;
    private String uri;

    private AffectedEntityBean() {
    }

    private AffectedEntityBean(AffectedEntityType type, String descriptionKey, Long numberOfOccurrences) {
        this(type, descriptionKey, numberOfOccurrences, null, null);
    }

    private AffectedEntityBean(AffectedEntityType type, String descriptionKey, Long numberOfOccurrences, String uriDisplayNameKey, String uri) {
        this.type = type;
        this.descriptionKey = descriptionKey;
        this.numberOfOccurrences = numberOfOccurrences;
        this.uriDisplayNameKey = uriDisplayNameKey;
        this.uri = uri;
    }

    @Nonnull
    static AffectedEntityBean from(@Nonnull final AffectedEntity affectedEntity) {
        final Long occurences = affectedEntity.getNumberOfOccurrences().orElse(null);

        if (affectedEntity.getLink().isPresent()) {
            final AffectedEntityLink link = affectedEntity.getLink().get();

            return new AffectedEntityBean(
                    affectedEntity.getType(),
                    affectedEntity.getDescriptionKey(),
                    occurences,
                    link.getDisplayNameKey(),
                    link.getUri().toString()
            );
        }
        return new AffectedEntityBean(affectedEntity.getType(), affectedEntity.getDescriptionKey(), occurences);
    }

    public AffectedEntityType getType() {
        return type;
    }

    public String getDescriptionKey() {
        return descriptionKey;
    }

    public Long getNumberOfOccurrences() {
        return numberOfOccurrences;
    }

    public String getUriDisplayNameKey() {
        return uriDisplayNameKey;
    }

    public String getUri() {
        return uri;
    }
}
