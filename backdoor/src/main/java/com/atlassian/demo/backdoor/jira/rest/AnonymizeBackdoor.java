package com.atlassian.demo.backdoor.jira.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AnonymizeUserService;
import com.atlassian.jira.user.anonymize.AnonymizeUserService.AnonymizePerformResult;
import com.atlassian.jira.user.anonymize.AnonymizeUserService.AnonymizeUserRequest;
import com.atlassian.jira.user.anonymize.AnonymizeUserService.AnonymizeValidationResult;
import com.atlassian.jira.user.anonymize.AnonymizeUserService.OperationsReport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

@Path("anonymize")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class AnonymizeBackdoor {
    private final AnonymizeUserService anonymizeUserService;
    private final JiraAuthenticationContext authenticationContext;

    public AnonymizeBackdoor(@ComponentImport AnonymizeUserService anonymizeUserService,
                             @ComponentImport JiraAuthenticationContext authenticationContext) {
        this.anonymizeUserService = anonymizeUserService;
        this.authenticationContext = authenticationContext;
    }

    @GET
    public Response getAffectedEntities(@QueryParam("userKey") String userKey) {
        final AnonymizeValidationResult validateResult = anonymizeUserService.preValidateAnonymize(
                AnonymizeUserRequest.builder()
                        .targetUser(userKey)
                        .executor(authenticationContext.getLoggedInUser())
                        .build()
        );
        if (!validateResult.isValid()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.ok(reportToAffectedEntities(validateResult.getAffectedEntitiesReport())).build();
    }

    private static Collection<AffectedEntityBean> reportToAffectedEntities(OperationsReport<Collection<AffectedEntity>> report) {
        if (report == null) {
            return Collections.emptyList();
        }

        return report.getReports().stream()
                .map(operationReport -> operationReport.getResult().get())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(AffectedEntityBean::from)
                .collect(Collectors.toList());
    }

    @POST
    public Response anonymize(@QueryParam("userKey") String userKey, @QueryParam("newOwnerKey") String newOwnerKey) {
        final AnonymizeValidationResult validateResult = anonymizeUserService.validateAnonymize(
                AnonymizeUserRequest.builder()
                        .targetUser(userKey)
                        .newOwnerKey(newOwnerKey)
                        .executor(authenticationContext.getLoggedInUser())
                        .build()
        );
        if (!validateResult.isValid()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        final AnonymizePerformResult performResult = anonymizeUserService.perform(validateResult, Contexts.nullContext());
        if (!performResult.getReport().isValid()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    performResult.getReport().toString()
            ).build();
        }

        return Response.ok(new AnonymizeResult(
                userKey,
                validateResult.getProcessData().getNewUserKey(),
                validateResult.getProcessData().getNewUserName())
        ).build();
    }

    @JsonAutoDetect
    public static class AnonymizeResult {
        @JsonProperty
        private final String userKey;

        @JsonProperty
        private final String newUserKey;

        @JsonProperty
        private final String newUserName;

        public AnonymizeResult(String userKey, String newUserKey, String newUserName) {
            this.userKey = userKey;
            this.newUserKey = newUserKey;
            this.newUserName = newUserName;
        }
    }
}
