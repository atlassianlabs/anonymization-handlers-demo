package com.atlassian.demo.backdoor.rest;

import com.atlassian.demo.api.Announcement;
import com.atlassian.demo.api.Watcher;
import com.atlassian.demo.api.WatchersManager;
import com.atlassian.demo.backdoor.rest.AnnouncementsBackdoor.AnnouncementRest;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("watcher")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class WatchersBackdoor {
    private final WatchersManager watchersManager;

    @Inject
    public WatchersBackdoor(@ComponentImport WatchersManager watchersManager) {
        this.watchersManager = watchersManager;
    }

    @GET
    @Path("{id}")
    public WatcherRest getWatch(@PathParam("id") int id) {
        final Optional<Watcher> watch = watchersManager.getWatch(id);
        return watch.map(WatcherRest::new).orElseGet(WatcherRest::new);
    }

    @GET
    public Collection<AnnouncementRest> getWatchedAnnouncementsForUser(@QueryParam("watcher") String userKey) {
        final Collection<Announcement> announcements = watchersManager.getWatchedAnnouncementsForUser(userKey);
        return announcements.stream().map(AnnouncementRest::new).collect(Collectors.toList());
    }

    @POST
    public WatcherRest addWatch(WatcherRest watcher) {
        final Watcher created = watchersManager.addWatcher(watcher.getWatcher(), watcher.getAnnouncementId());
        return getWatch(created.getId());
    }

    @JsonAutoDetect
    public static class WatcherRest {
        private Integer id;
        private String watcher;
        private Integer announcementId;

        // for Jackson
        private WatcherRest() {
        }

        public WatcherRest(Watcher watcher) {
            this.id = watcher.getId();
            this.watcher = watcher.getWatcher();
            this.announcementId = watcher.getAnnouncementId();
        }

        public Integer getId() {
            return id;
        }

        public String getWatcher() {
            return watcher;
        }

        public Integer getAnnouncementId() {
            return announcementId;
        }
    }
}
