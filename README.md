# User anonymization handlers demo

This is a demo project, showing how to implement user anonymization handlers for Jira Server:

 - in a cross-product app,
 - supporting multiple product versions with a single build,
 - with end-to-end tests.

## Running

First, run `mvn clean install -DskipTests` in the main directory.

Then, from the `plugin` directory execute:

`mvn amps:debug -Dproduct=APPLICATION -DskipTests`, e.g. `mvn amps:debug -Dproduct=refapp -DskipTests` or `mvn amps:debug -Dproduct=jira -DskipTests`

Note: you might need to run `mvn clean` or `rm -rf target` between running with different products due to Tomcat version mismatches.

## Integration tests

1. Start the selected product like above.
2. In the `integration-tests` directory run the tests **passing additional `-Dproduct=<PRODUCT_NAME>` parameter**, e.g. `mvn integration-test -Dproduct=jira`.

Tests can be also run directly from the IDE, as long as the aforementioned parameter is provided.

Note: The tests in `com.atlassian.demo.jira` package will still run and fail for any other product then Jira.
I didn't have enough time to play with Maven to make it work properly, sorry!

## Contributing

This project is not set up for accepting external contributions.
